package es.ucm.fdi.view;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

public class PanelTabla<T> extends JPanel {
	private ModeloTabla<T> modelo;

	public PanelTabla(String bordeId, ModeloTabla<T> modelo) {
		setBorder(new TitledBorder(bordeId));
		setLayout(new GridLayout(1, 1));
		this.modelo = modelo;
		JTable tabla = new JTable(modelo);
		add(new JScrollPane(tabla, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS));
	}
}