package es.ucm.fdi.model.ObjetosSimulacion;

import es.ucm.fdi.ini.IniSection;

public class CruceCongestionado extends CruceGenerico<CarreteraEntranteConIntervalo> {
	public CruceCongestionado(String id) {
		super(id);
	}

	@Override
	protected CarreteraEntranteConIntervalo creaCarreteraEntrante(Carretera carretera) {
		return new CarreteraEntranteConIntervalo(carretera, 0);
	}

	@Override
	protected void actualizaSemaforos() {
		if (indiceSemaforoVerde == -1){
			CarreteraEntranteConIntervalo c = carreterasEntrantes.get(0);
			for (CarreteraEntranteConIntervalo carreterasEntrante : carreterasEntrantes) {
				if (carreterasEntrante.colaVehiculos.size() > c.colaVehiculos.size()){
					c = carreterasEntrante;
				}
			}
			c.semaforo = true;
			c.setIntervaloDeTiempo(Math.max(c.colaVehiculos.size() / 2, 1));
			indiceSemaforoVerde = carreterasEntrantes.indexOf(c);
		} else {
			CarreteraEntranteConIntervalo c = carreterasEntrantes.get(indiceSemaforoVerde);
			CarreteraEntranteConIntervalo siguiente = null;
			if (c.tiempoConsumido()){
				c.semaforo = false;
				c.setUnidadesDeTiempoUsadas(0);
				c.setUsoCompleto(false);
				int cantidadC = 0;
				for (CarreteraEntranteConIntervalo carreterasEntrante : carreterasEntrantes) {
					if (carreterasEntrante.colaVehiculos.size() >= cantidadC &&
							c != carreterasEntrante){
						siguiente = carreterasEntrante;
						cantidadC = carreterasEntrante.colaVehiculos.size();
					}
				}
				if (siguiente == null){
					c.setUnidadesDeTiempoUsadas(0);
					c.semaforo = true;
					return;
				}
				indiceSemaforoVerde = carreterasEntrantes.indexOf(siguiente);
				siguiente.semaforo = true;
				siguiente.setIntervaloDeTiempo(Math.max(c.colaVehiculos.size() / 2, 1));
				siguiente.setUnidadesDeTiempoUsadas(0);
			}
		}
	}

	@Override
	protected void completaDetallesSeccion(IniSection is) {
		String queues = "";

		for (CarreteraEntranteConIntervalo c : carreterasEntrantes) {
			queues += "(" + c.carretera.getId() + "," +
					(c.semaforo == true ? "green:" + (c.getIntervaloDeTiempo() - c.getUnidadesDeTiempoUsadas())
							: "red") + ",[";
			for (Vehiculo v : c.colaVehiculos) {
				queues += v.getId();
				if (!v.equals(c.colaVehiculos.get(c.colaVehiculos.size() - 1))) {
					queues += ",";
				}
			}

			queues += "])" + (!c.equals(carreterasEntrantes.get(carreterasEntrantes.size() - 1)) ? "," : "");

		}
		is.setValue("queues", queues);
		is.setValue("type", "mc");
	}
}
