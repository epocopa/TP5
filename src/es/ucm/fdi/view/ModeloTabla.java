package es.ucm.fdi.view;

import es.ucm.fdi.control.Controlador;
import es.ucm.fdi.model.ObservadorSimuladorTrafico;

import javax.swing.table.DefaultTableModel;
import java.util.List;

public abstract class ModeloTabla<T> extends DefaultTableModel implements ObservadorSimuladorTrafico {
	protected String[] columnIds;
	protected List<T> lista;

	public ModeloTabla(String[] columnIds, Controlador ctrl) {
		lista = null;
		this.columnIds = columnIds;
		ctrl.addObservador(this);
	}

	@Override
	public String getColumnName(int col) {
		return columnIds[col];
	}

	@Override
	public int getColumnCount() {
		return columnIds.length;
	}

	@Override
	public int getRowCount() {
		return lista == null ? 0 : lista.size();
	}
}