package es.ucm.fdi.view;

import es.ucm.fdi.control.Controlador;
import es.ucm.fdi.control.ErrorDeSimulacion;
import es.ucm.fdi.model.MapaCarreteras;
import es.ucm.fdi.model.ObjetosSimulacion.Carretera;
import es.ucm.fdi.model.events.Evento;

import java.util.List;

public class ModeloTablaCarreteras extends ModeloTabla<Carretera> {
	public ModeloTablaCarreteras(String[] columnIdCarretera, Controlador controlador) {
		super(columnIdCarretera, controlador);
	}

	@Override
	public Object getValueAt(int indiceFil, int indiceCol) {
		Object s = null;
		switch (indiceCol) {
			case 0:
				s = lista.get(indiceFil).getId();
				break;
			case 1:
				s = lista.get(indiceFil).getCruceOrigen().getId();
				break;
			case 2:
				s = lista.get(indiceFil).getCruceDestino().getId();
				break;
			case 3:
				s = lista.get(indiceFil).getLongitud();
				break;
			case 4:
				s = lista.get(indiceFil).getVelocidadMaxima();
				break;
			case 5:
				s = lista.get(indiceFil).completarVehiculos();
				break;
			default:
				assert (false);
		}
		return s;
	}

	@Override
	public void errorSimulador(int tiempo, MapaCarreteras map, List<Evento> eventos, ErrorDeSimulacion e) {

	}

	@Override
	public void avanza(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {
		lista = mapa.getCarreteras();
		fireTableStructureChanged();
	}

	@Override
	public void addEvento(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {

	}

	@Override
	public void reinicia(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {
		lista = null;
		fireTableStructureChanged();
	}
}
