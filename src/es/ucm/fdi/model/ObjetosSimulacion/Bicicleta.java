package es.ucm.fdi.model.ObjetosSimulacion;

import es.ucm.fdi.ini.IniSection;

import java.util.List;

public class Bicicleta extends Vehiculo {
	public Bicicleta(String id, int velocidadMaxima, List<CruceGenerico<?>> itinerario) {
		super(id, velocidadMaxima, itinerario);
	}

	@Override
	public void setTiempoAveria(int tiempoAveria) {
		if (velocidadActual >= velocidadActual / 2)
			super.setTiempoAveria(tiempoAveria);
	}

	@Override
	public void completaDetallesSeccion(IniSection is) {
		super.completaDetallesSeccion(is);
		is.setValue("type", "bike");
	}
}
