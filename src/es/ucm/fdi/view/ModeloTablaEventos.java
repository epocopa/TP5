package es.ucm.fdi.view;

import es.ucm.fdi.control.Controlador;
import es.ucm.fdi.control.ErrorDeSimulacion;
import es.ucm.fdi.model.MapaCarreteras;
import es.ucm.fdi.model.events.Evento;

import java.util.List;

public class ModeloTablaEventos extends ModeloTabla<Evento> {
	public ModeloTablaEventos(String[] columnIdEventos, Controlador ctrl) {
		super(columnIdEventos, ctrl);
	}

	@Override // necesario para que se visualicen los datos
	public Object getValueAt(int indiceFil, int indiceCol) {
		Object s = null;
		switch (indiceCol) {
			case 0:
				s = indiceFil;
				break;
			case 1:
				s = lista.get(indiceFil).getTiempo();
				break;
			case 2:
				s = lista.get(indiceFil).toString();
				break;
			default:
				assert (false);
		}
		return s;
	}

	@Override
	public void errorSimulador(int tiempo, MapaCarreteras map, List<Evento> eventos, ErrorDeSimulacion e) {

	}

	@Override
	public void avanza(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {
		lista = eventos;
		lista.removeIf(p-> p.getTiempo() < tiempo);
		fireTableStructureChanged();
	}

	@Override
	public void addEvento(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {
		lista = eventos;
		lista.removeIf(p-> p.getTiempo() < tiempo);
		fireTableStructureChanged();
	}

	@Override
	public void reinicia(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {
		lista = null;
		fireTableStructureChanged();
	}
}