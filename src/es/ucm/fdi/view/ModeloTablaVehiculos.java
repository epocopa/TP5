package es.ucm.fdi.view;

import es.ucm.fdi.control.Controlador;
import es.ucm.fdi.control.ErrorDeSimulacion;
import es.ucm.fdi.model.MapaCarreteras;
import es.ucm.fdi.model.ObjetosSimulacion.Vehiculo;
import es.ucm.fdi.model.events.Evento;

import java.util.List;

public class ModeloTablaVehiculos extends ModeloTabla<Vehiculo> {
	public ModeloTablaVehiculos(String[] columnIdVehiculo, Controlador controlador) {
		super(columnIdVehiculo, controlador);
	}

	@Override
	public Object getValueAt(int indiceFil, int indiceCol) {
		Object s = null;
		switch (indiceCol) {
			case 0:
				s = lista.get(indiceFil).getId();
				break;
			case 1:
				if (lista.get(indiceFil).getCarretera() != null) {
					s = lista.get(indiceFil).getCarretera().getId();
				}
				break;
			case 2:
				s = lista.get(indiceFil).getLocalizacion();
				break;
			case 3:
				s = lista.get(indiceFil).getVelocidadActual();
				break;
			case 4:
				s = lista.get(indiceFil).getKilometraje();
				break;
			case 5:
				s = lista.get(indiceFil).getTiempoAveria();
				break;
			case 6:
				s = lista.get(indiceFil).mostrarItinerario();
				break;
			default:
				assert (false);
		}
		return s;
	}

	@Override
	public void errorSimulador(int tiempo, MapaCarreteras map, List<Evento> eventos, ErrorDeSimulacion e) {

	}

	@Override
	public void avanza(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {
		lista = mapa.getVehiculos();
		fireTableStructureChanged();
	}

	@Override
	public void addEvento(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {

	}

	@Override
	public void reinicia(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {
		lista = null;
		fireTableStructureChanged();
	}
}
