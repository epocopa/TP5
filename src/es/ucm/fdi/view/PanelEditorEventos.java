package es.ucm.fdi.view;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class PanelEditorEventos extends PanelAreaTexto {
	public PanelEditorEventos(String titulo, String texto, boolean editable, MainView mainView) {
		super(titulo, editable);
		setTexto(texto);
		PopUpMenu popUp = new PopUpMenu(mainView);
		areatexto.add(popUp);
		areatexto.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger() && areatexto.isEnabled()) {
					popUp.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}
		});
	}
}
