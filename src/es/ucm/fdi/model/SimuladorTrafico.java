package es.ucm.fdi.model;

import es.ucm.fdi.model.events.Evento;
import es.ucm.fdi.util.SortedArrayList;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SimuladorTrafico implements Observador<ObservadorSimuladorTrafico>{
	private MapaCarreteras mapa;
	private List<Evento> eventos;
	private int contadorTiempo;
	private List<ObservadorSimuladorTrafico> observadores;

	public SimuladorTrafico() {
		mapa = new MapaCarreteras();
		contadorTiempo = 0;
		Comparator<Evento> cmp = new Comparator<Evento>() {
			@Override
			public int compare(Evento evento, Evento t1) {
				if (evento.getTiempo() == t1.getTiempo()){
					return 0;
				} else if (evento.getTiempo() < t1.getTiempo()){
					return -1;
				} else {
					return 1;
				}
			}
		};
		eventos = new SortedArrayList<>(cmp); // estructura ordenada por “tiempo”
		observadores = new ArrayList<>();
	}

	public void ejecuta(int pasosSimulacion, OutputStream ficheroSalida) {
		int limiteTiempo = contadorTiempo + pasosSimulacion - 1;
		while ( contadorTiempo <= limiteTiempo ) {
			for (Evento e : eventos){
				if (e.getTiempo() == contadorTiempo){
					e.ejecuta(mapa);
				}
			}
			contadorTiempo++;
			mapa.actualizar();
			for (ObservadorSimuladorTrafico observador : observadores) {
				observador.avanza(contadorTiempo, mapa, eventos);
			}
			//PrintWriter y try-with-resources?
			try (PrintWriter p = new PrintWriter(ficheroSalida)) {
				p.print(generarInformes());
			}
		}
	}

	public void insertaEvento(Evento e) {
		//No hace falta excepcion porque Parsea int no negativo
		eventos.add(e);
		for (ObservadorSimuladorTrafico observador : observadores) {
			observador.addEvento(contadorTiempo, mapa, eventos);
		}
	}

	public String generarInformes(){
		return mapa.generateReport(contadorTiempo);
	}

	public void reiniciar() {
		mapa = new MapaCarreteras();
		contadorTiempo = 0;
		Comparator<Evento> cmp = new Comparator<Evento>() {
			@Override
			public int compare(Evento evento, Evento t1) {
				if (evento.getTiempo() == t1.getTiempo()){
					return 0;
				} else if (evento.getTiempo() < t1.getTiempo()){
					return -1;
				} else {
					return 1;
				}
			}
		};
		eventos = new SortedArrayList<>(cmp); // estructura ordenada por “tiempo”

		for (ObservadorSimuladorTrafico observador : observadores) {
			observador.reinicia(contadorTiempo, mapa, eventos);
		}
	}

	@Override
	public void addObservador(ObservadorSimuladorTrafico o) {
		if (o != null && !this.observadores.contains(o)) {
			this.observadores.add(o);
		}
	}
	
	@Override
	public void removeObservador(ObservadorSimuladorTrafico o) {
		if (o != null && this.observadores.contains(o)) {
			this.observadores.remove(o);
		}
	}
}
