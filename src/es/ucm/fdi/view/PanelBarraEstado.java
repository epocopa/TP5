package es.ucm.fdi.view;

import es.ucm.fdi.control.Controlador;
import es.ucm.fdi.control.ErrorDeSimulacion;
import es.ucm.fdi.model.MapaCarreteras;
import es.ucm.fdi.model.ObservadorSimuladorTrafico;
import es.ucm.fdi.model.events.Evento;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class PanelBarraEstado extends JPanel implements ObservadorSimuladorTrafico{
	private JLabel statusBarText;

	public PanelBarraEstado(String texto, Controlador controlador){
		super(new FlowLayout(FlowLayout.LEFT));
		statusBarText = new JLabel(texto);
		add(statusBarText);
		setBorder(BorderFactory.createLoweredBevelBorder());
		controlador.addObservador(this);
	}

	public void setMensaje(String mensaje ){
		statusBarText.setText(mensaje);
	}

	@Override
	public void errorSimulador(int tiempo, MapaCarreteras map, List<Evento> eventos, ErrorDeSimulacion e) {

	}

	@Override
	public void avanza(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {

	}

	@Override
	public void addEvento(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {

	}

	@Override
	public void reinicia(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {

	}
}
