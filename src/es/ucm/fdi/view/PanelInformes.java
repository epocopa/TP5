package es.ucm.fdi.view;

import es.ucm.fdi.control.Controlador;
import es.ucm.fdi.control.ErrorDeSimulacion;
import es.ucm.fdi.model.MapaCarreteras;
import es.ucm.fdi.model.ObservadorSimuladorTrafico;
import es.ucm.fdi.model.events.Evento;

import java.util.List;

public class PanelInformes extends PanelAreaTexto implements ObservadorSimuladorTrafico {

	private boolean redirigir;
	Controlador controlador;

	public PanelInformes(String titulo, boolean editable, Controlador ctrl) {
		super(titulo, editable);
		controlador = ctrl;
		controlador.addObservador(this); // se añade como observador
	}

	public void setRedirigir(boolean redirigir) {
		this.redirigir = redirigir;
	}

	@Override
	public void errorSimulador(int tiempo, MapaCarreteras map, List<Evento> eventos, ErrorDeSimulacion e) {

	}

	@Override
	public void avanza(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {
		if (redirigir){
			areatexto.append(controlador.generarInformes());
		}
	}

	@Override
	public void addEvento(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {

	}

	@Override
	public void reinicia(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {
		setTexto("");
	}
}