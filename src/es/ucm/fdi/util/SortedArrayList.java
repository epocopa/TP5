package es.ucm.fdi.util;

import es.ucm.fdi.control.ErrorDeSimulacion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

public class SortedArrayList<E> extends ArrayList<E> {
	private Comparator<E> cmp;

	public SortedArrayList(Comparator<E> cmp) {
		this.cmp = cmp;
	}

	@Override
	public boolean add(E e) {
		if (size() == 0){
			super.add(0, e);
			return true;
		}
		for (int i = 0; i < this.size(); i++) {
			if(cmp.compare(e, this.get(i)) == -1){
				super.add(i, e);
				return true;
			} else {
				super.add(size(), e);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends E> collection) {
		for (E e : collection){
			add(e);
		}
		return true;
	}

	@Override
	public E set(int i, E e) {
		throw  new ErrorDeSimulacion("Operacion no permitda");
	}

	@Override
	public void add(int i, E e) {
		throw  new ErrorDeSimulacion("Operacion no permitda");
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		throw  new ErrorDeSimulacion("Operacion no permitda");
	}

}
