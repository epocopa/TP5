package es.ucm.fdi.constructor;

import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.model.events.Evento;
import es.ucm.fdi.model.events.EventoAveriaCoche;

public class ConstructorEventoAveriaCoche extends ConstructorEventos {

	public ConstructorEventoAveriaCoche() {
		etiqueta = "make_vehicle_faulty";
		claves = new String[]{"time", "id"};
		valoresPorDefecto = new String[]{"0", "0"};
	}

	@Override
	public Evento parser(IniSection section) {
		if (!section.getTag().equals(etiqueta) || section.getValue("type") != null) {
			return null;
		} else {
			return new EventoAveriaCoche(
					parseaIntNoNegativo(section, "duration", 0),
					section.getValue("vehicles").split(","),
					parseaIntNoNegativo(section, "time", 0)
			);
		}
	}

	@Override
	public String toString() {
		return "Nueva averia";
	}
}
