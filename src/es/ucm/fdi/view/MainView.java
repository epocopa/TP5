package es.ucm.fdi.view;


import es.ucm.fdi.control.Controlador;
import es.ucm.fdi.control.ErrorDeSimulacion;
import es.ucm.fdi.model.MapaCarreteras;
import es.ucm.fdi.model.ObjetosSimulacion.Carretera;
import es.ucm.fdi.model.ObjetosSimulacion.CruceGenerico;
import es.ucm.fdi.model.ObjetosSimulacion.Vehiculo;
import es.ucm.fdi.model.ObservadorSimuladorTrafico;
import es.ucm.fdi.model.events.Evento;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.List;

public class MainView extends JFrame implements ObservadorSimuladorTrafico {
	static private final String[] columnIdEventos = {"#", "Tiempo", "Tipo"};
	static private final String[] columnIdVehiculo = {"ID", "Carretera",
			"Localizacion", "Vel.", "Km", "Tiempo. Ave.", "Itinerario"};
	static private final String[] columnIdCarretera = {"ID", "Origen",
			"Destino", "Longitud", "Vel. Max", "Vehiculos"};
	static private final String[] columnIdCruce = {"ID", "Verde", "Rojo"};

	private ToolBar toolBar;
	private BarraMenu barraMenu;

	private PanelEditorEventos panelEditorEventos;
	private PanelInformes panelInformes;
	private PanelTabla<Evento> panelColaEventos;

	private PanelTabla<Vehiculo> panelVehiculos;
	private PanelTabla<Carretera> panelCarreteras;
	private PanelTabla<CruceGenerico<?>> panelCruces;

	private ComponenteMapa componenteMapa;
	private PanelBarraEstado panelBarraEstado;

	private JFileChooser fc;
	private File ficheroActual;
	private Controlador controlador;

	public MainView(String ficheroEntrada, Controlador controlador) throws FileNotFoundException {
		ficheroActual = ficheroEntrada != null ? new File(ficheroEntrada) : null;
		this.controlador = controlador;
		controlador.addObservador(this);

		createView();

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new ConfirmarCerrar());
		setTitle("Traffic Simulator");
		setResizable(true);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	private void createView() {
		JPanel mainPanel = new JPanel(new BorderLayout());
		setContentPane(mainPanel);

		JPanel panelCentral = new JPanel();
		//GRID
		panelCentral.setLayout(new GridLayout(2, 1));
		//BOX
		//panelCentral.setLayout(new BoxLayout(panelCentral, BoxLayout.Y_AXIS));
		mainPanel.add(panelCentral, BorderLayout. CENTER);

		//Toolbar
		toolBar = new ToolBar(this, controlador);
		mainPanel.add(toolBar, BorderLayout.NORTH);

		//Top Row
		panelCentral.add(topRowBuilder());

		//Bottom Row
		panelCentral.add(bottomRowBuilder());

		//Status bar
		panelBarraEstado = new PanelBarraEstado("Bienvenid@ al simulador!", controlador);
		mainPanel.add(panelBarraEstado, BorderLayout.SOUTH);

		//Menu
		barraMenu = new BarraMenu(this, controlador);
		setJMenuBar(barraMenu);

		fc = new JFileChooser("resources/examples/events/");
	}

	private JPanel topRowBuilder() {
		JPanel topRow = new JPanel(new GridLayout(1, 3));

		panelEditorEventos = new PanelEditorEventos("Eventos: ", "",
				true, this);
		panelColaEventos = new PanelTabla<Evento>("Cola Eventos: ",
				new ModeloTablaEventos(columnIdEventos, controlador));
		panelInformes = new PanelInformes("Informes: ", false, controlador);

		topRow.add(panelEditorEventos);
		topRow.add(panelColaEventos);
		topRow.add(panelInformes);

		return topRow;
	}

	private JPanel bottomRowBuilder() {
		JPanel bottomRow = new JPanel();
		bottomRow.setLayout(new GridLayout(1, 2));

		JPanel panelTablas = new JPanel();
		panelTablas.setLayout(new GridLayout(3, 1));
		bottomRow.add(panelTablas);


		//Vehicles table
		panelVehiculos = new PanelTabla<Vehiculo>( "Vehiculos",
				new ModeloTablaVehiculos(columnIdVehiculo, controlador ));
		panelTablas.add(panelVehiculos);

		//Roads table
		panelCarreteras = new PanelTabla<Carretera>( "Carretras",
				new ModeloTablaCarreteras(columnIdCarretera, controlador));
		panelTablas.add(panelCarreteras);

		//Junctions table
		panelCruces = new PanelTabla<CruceGenerico<?>>( "Cruces",
				new ModeloTablaCruces(columnIdCruce, controlador));
		panelTablas.add(panelCruces);

		//Mapa
		componenteMapa = new ComponenteMapa(controlador);
		bottomRow.add(componenteMapa);

		return bottomRow;
	}

	public void cargaFichero() {
		if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			File fichero = fc.getSelectedFile();
			String s = leeFichero(fichero);
			controlador.reinicia();
			ficheroActual = fichero;
			panelEditorEventos.setTexto(s);
			panelEditorEventos.setBorde(ficheroActual.getName());
			informar("Fichero " + fichero.getName() + " de eventos cargado en el editor");
		}
	}

	private String leeFichero(File fichero) {
		StringBuilder sb = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader(fichero))) {
			String linea;
			while ((linea = br.readLine()) != null) {
				sb.append(linea);
				sb.append("\n");
			}
		} catch (FileNotFoundException e) {
			muestraDialogoError("Error durante la lectura del fichero: " + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public void guardarInformes(){
		if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
			File fichero = fc.getSelectedFile();
			escribirFichero(panelInformes.getTexto() ,fichero);
		}
	}

	public void guardarEventos(){
		if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
			File fichero = fc.getSelectedFile();
			escribirFichero(panelEditorEventos.getTexto() ,fichero);
		}
	}

	public void escribirFichero(String s, File fichero){
		try (PrintWriter p = new PrintWriter(fichero)) {
			p.print(s);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void muestraDialogoError(String s) {
		JOptionPane.showMessageDialog(null, s, "Error", JOptionPane.ERROR_MESSAGE);
	}

	public String getTextoEditorEventos(){
		return panelEditorEventos.getTexto();
	}

	public int getSteps() {
		return toolBar.getSteps();
	}

	public void clearReports(){
		panelInformes.limpiar();
	}
	public void clearEditor(){
		panelEditorEventos.limpiar();
	}

	public void informar(String s){
		panelBarraEstado.setMensaje(s);
	}

	public void insertarPanelEventos(String s){
		panelEditorEventos.insertar(s);
	}

	public void setRedirigir(boolean redirigir){
		panelInformes.setRedirigir(redirigir);
	}

	public void setTextoPanelEInformes(String s){
		panelInformes.setTexto(s);
	}

	@Override
	public void errorSimulador(int tiempo, MapaCarreteras map, List<Evento> eventos, ErrorDeSimulacion e) {

	}

	@Override
	public void avanza(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {

	}

	@Override
	public void addEvento(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {

	}

	@Override
	public void reinicia(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {

	}
}
