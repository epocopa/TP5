package es.ucm.fdi.view;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

abstract public class PanelAreaTexto extends JPanel {
	protected JTextArea areatexto;

	public PanelAreaTexto(String titulo, boolean editable) {
		areatexto = new JTextArea();
		setLayout(new GridLayout(1, 1));
		areatexto.setEditable(editable);
		add(new JScrollPane(areatexto, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS));
		setBorde(titulo);
	}

	public void setBorde(String titulo) {
		this.setBorder(new TitledBorder(titulo));
	}

	public String getTexto() {
		return areatexto.getText();
	}

	public void setTexto(String texto) {
		areatexto.setText(texto);
	}

	public void limpiar() {
		areatexto.setText("");
	}

	public void insertar(String valor) {
		areatexto.insert(valor, areatexto.getCaretPosition());
	}
}
