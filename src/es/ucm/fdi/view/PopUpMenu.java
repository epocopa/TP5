package es.ucm.fdi.view;

import es.ucm.fdi.constructor.ConstructorEventos;
import es.ucm.fdi.util.ParserEventos;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PopUpMenu extends JPopupMenu {

	public PopUpMenu(MainView mainView) {
		JMenu plantillas = new JMenu("Nueva plantilla");
		add(plantillas);

		for (ConstructorEventos ce : ParserEventos.getConstrutoresEventos()) {
			JMenuItem mi = new JMenuItem(ce.toString());
			mi.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					mainView.insertarPanelEventos(ce.template() + System.lineSeparator());
				}
			});
			plantillas.add(mi);
		}
	}
}
