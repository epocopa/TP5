package es.ucm.fdi.model.ObjetosSimulacion;

import es.ucm.fdi.ini.IniSection;

import java.util.List;
import java.util.Random;

public class Coche extends Vehiculo {
	protected int kmUltimaAveria;
	protected int resistenciaKm;
	protected int duracionMaximaAveria;
	protected double probabilidadDeAveria;
	protected Random numAleatorio;

	public Coche(String id , int velocidadMaxima , int resistenciaKm , double probabilidad , long semilla , int duracionMaximaAveria , List<CruceGenerico<?>> itinerario ) {
		super(id, velocidadMaxima, itinerario);
		this.resistenciaKm = resistenciaKm;
		this.duracionMaximaAveria = duracionMaximaAveria;
		this.probabilidadDeAveria = probabilidad;
		this.numAleatorio = new Random(semilla);
	}

	@Override
	public void avanza() {
		if (tiempoAveria > 0) {
			kmUltimaAveria = kilometraje;
		} else if (kilometraje -kmUltimaAveria > resistenciaKm && numAleatorio.nextDouble() < probabilidadDeAveria) {
			setTiempoAveria(numAleatorio.nextInt(duracionMaximaAveria) + 1);
		}
		super.avanza();
	}

	@Override
	public void completaDetallesSeccion(IniSection is) {
		super.completaDetallesSeccion(is);
		is.setValue("type", "car");
	}
}
