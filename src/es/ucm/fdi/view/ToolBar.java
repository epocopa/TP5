package es.ucm.fdi.view;

import es.ucm.fdi.control.Controlador;
import es.ucm.fdi.control.ErrorDeSimulacion;
import es.ucm.fdi.model.MapaCarreteras;
import es.ucm.fdi.model.ObservadorSimuladorTrafico;
import es.ucm.fdi.model.events.Evento;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.util.List;

public class ToolBar extends JToolBar implements ObservadorSimuladorTrafico {

	JSpinner steps;
	JTextField time;

	public ToolBar(MainView mainView, Controlador controlador) {
		super();
		controlador.addObservador(this);

		JButton load = new JButton(new ImageIcon("resources/icons/load.png"));
		load.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				mainView.cargaFichero();
			}
		});

		JButton save = new JButton(new ImageIcon("resources/icons/save.png"));
		save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				mainView.guardarEventos();
				mainView.informar("Texto de Eventos guardados");
			}
		});

		JButton clear = new JButton(new ImageIcon("resources/icons/clear.png"));
		clear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				mainView.clearEditor();
				mainView.informar("Editor limpiado");
			}
		});

		JButton cal = new JButton(new ImageIcon("resources/icons/cal.png"));
		JButton play = new JButton(new ImageIcon("resources/icons/play.png"));
		cal.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				play.setEnabled(true);
				try {
					controlador.reinicia();
					byte[] contenido = mainView.getTextoEditorEventos().getBytes();
					controlador.cargarEventos(new ByteArrayInputStream(contenido));
				} catch (ErrorDeSimulacion err) {
				}
				mainView.informar("Eventos cargados al simulador!");
			}
		});

		play.setEnabled(false);
		play.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				controlador.ejecuta(getSteps());
				mainView.informar(getSteps() == 1 ? "Ejecutado " + getSteps() + " paso"
						: "Ejecutados " + getSteps() + " pasos");
			}
		});

		JButton reset = new JButton(new ImageIcon("resources/icons/reset.png"));
		reset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				play.setEnabled(false);
				controlador.reinicia();
				mainView.informar("Simulador reiniciado");
			}
		});

		JButton generate = new JButton(new ImageIcon("resources/icons/generate.png"));
		generate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				mainView.setTextoPanelEInformes(controlador.generarInformes());
				mainView.informar("Informes generados");
			}
		});

		JButton clearR = new JButton(new ImageIcon("resources/icons/clearR.png"));
		clearR.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				mainView.clearReports();
				mainView.informar("Panel informes limpiado");
			}
		});

		JButton saveR = new JButton(new ImageIcon("resources/icons/saveR.png"));
		saveR.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				mainView.guardarInformes();
				mainView.informar("Texto de Informes guardados");
			}
		});

		JButton exit = new JButton(new ImageIcon("resources/icons/exit.png"));
		exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				System.exit(0);
			}
		});


		JLabel stepsLabel = new JLabel("  Pasos:  ");
		JLabel timeLabel = new JLabel("  Tiempo:  ");

		steps = new JSpinner();
		steps.setValue(1);
		steps.setMaximumSize(new Dimension(60, 30));
		time = new JTextField("0");
		time.setEditable(false);
		time.setMaximumSize(new Dimension(60, 30));

		load.setBorderPainted(false);
		save.setBorderPainted(false);
		clear.setBorderPainted(false);
		cal.setBorderPainted(false);
		play.setBorderPainted(false);
		reset.setBorderPainted(false);
		generate.setBorderPainted(false);
		clearR.setBorderPainted(false);
		saveR.setBorderPainted(false);
		exit.setBorderPainted(false);


		add(load);
		add(save);
		add(clear);
		addSeparator();
		add(cal);
		add(play);
		add(reset);
		add(stepsLabel);
		add(steps);
		add(timeLabel);
		add(time);
		addSeparator();
		add(generate);
		add(clearR);
		add(saveR);
		addSeparator();
		add(exit);
	}

	public int getSteps() {
		return (int) steps.getValue();
	}

	@Override
	public void errorSimulador(int tiempo, MapaCarreteras map, List<Evento> eventos, ErrorDeSimulacion e) {

	}

	@Override
	public void avanza(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {
		time.setText(String.valueOf(tiempo));
	}

	@Override
	public void addEvento(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {

	}

	@Override
	public void reinicia(int tiempo, MapaCarreteras mapa, List<Evento> eventos) {
		time.setText("");
		steps.setValue(1);
	}
}
