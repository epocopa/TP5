package es.ucm.fdi.model.ObjetosSimulacion;

import es.ucm.fdi.ini.IniSection;

public class Autopista extends Carretera {
	private int numCarriles;

	public Autopista(String id, int longitud, int velocidadMaxima, CruceGenerico<?> cruceOrigen, CruceGenerico<?> cruceDestino, int numCarriles) {
		super(id, longitud, velocidadMaxima, cruceOrigen, cruceDestino);
		this.numCarriles = numCarriles;
	}

	@Override
	protected int calculaVelocidadBase() {
		return Math.min(velocidadMaxima, ( (velocidadMaxima * numCarriles) / (Math.max(vehiculos.size(),1)) ) + 1);
	}

	@Override
	protected int calculaFactorReduccion(int obstacles) {
		return obstacles < numCarriles ? 1 : 2;
	}

	@Override
	protected void completaDetallesSeccion(IniSection is) {
		super.completaDetallesSeccion(is);
		is.setValue("type", "lanes");
	}
}
