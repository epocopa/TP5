package es.ucm.fdi.view;

import es.ucm.fdi.control.Controlador;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class BarraMenu extends JMenuBar {

	public BarraMenu(MainView mainView, Controlador controlador) {
		JMenu file = new JMenu("File");
		JMenu simulator = new JMenu("Simulator");
		JMenu reports = new JMenu("Reports");

		add(file);
		add(simulator);
		add(reports);

		JMenuItem load = new JMenuItem("Load Events");
		load.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				mainView.cargaFichero();
			}
		});

		JMenuItem save = new JMenuItem("Save Events");
		save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				mainView.guardarEventos();
			}
		});

		JMenuItem saveR = new JMenuItem("Save Report");
		saveR.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				mainView.guardarInformes();
			}
		});

		JMenuItem exit = new JMenuItem("Exit");
		exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				System.exit(0);
			}
		});

		JMenuItem run = new JMenuItem("Run");
		run.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				controlador.ejecuta(mainView.getSteps());
				mainView.informar(mainView.getSteps() == 1 ? "Ejecutado " + mainView.getSteps() + " paso"
						: "Ejecutados " + mainView.getSteps() + " pasos");
			}
		});

		JMenuItem reset = new JMenuItem("Reset");
		reset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				controlador.reinicia();
				mainView.informar("Simulador reiniciado");
			}
		});

		JCheckBoxMenuItem redir = new JCheckBoxMenuItem("Redirect Output");
		redir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				mainView.setRedirigir(redir.getState());
			}
		});

		JMenuItem generate = new JMenuItem("Generate");
		generate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				mainView.setTextoPanelEInformes(controlador.generarInformes());
				mainView.informar("Informes generados");
			}
		});

		JMenuItem clear = new JMenuItem("Clear");
		clear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				mainView.clearReports();
				mainView.informar("Panel informes limpiado");
			}
		});


		file.add(load);
		file.add(save);
		file.addSeparator();
		file.add(saveR);
		file.addSeparator();
		file.add(exit);

		simulator.add(run);
		simulator.add(reset);
		simulator.add(redir);

		reports.add(generate);
		reports.add(clear);


		load.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK));
		save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		saveR.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
	}
}
